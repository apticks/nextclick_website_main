<!--Add Category And its list-->


		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Contacts</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Contact Name</th>
									<th>Email</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
							<?php if(!empty($contacts)):?>
    							<?php  $sno = 1; foreach ($contacts as $con): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $con['name'];?></td>
									<td><?php echo $con['email'];?></td>
									
									<td><a
										href="<?php echo base_url()?>category/edit?id=<?php echo $category['id']; ?>"
										class=" mr-2  " type="category"> <i class="fas fa-pencil-alt"></i>
									</a> <a href="#" class="mr-2  text-danger "
										onClick="delete_record(<?php echo $category['id'] ?>, 'category')">
											<i class="far fa-trash-alt"></i>
									</a></td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='5'><h3>
											<center>No Contacts</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>
