<?php

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->template = 'template/master/main';
        $this->load->model('contact_model');
        $this->load->model('portfolio_details_model');
         $this->load->model('Subscribe_model');
    }
    
    public function index(){
        $this->data['title'] = 'home';
        $this->data['content'] = 'home/home';
        $this->_render_page($this->template, $this->data);
    }
     public function about_us(){
        $this->data['title'] = 'about_us';
        $this->data['content'] = 'home/about_us';
        $this->_render_page($this->template, $this->data);
    }
    public function faq(){
        $this->data['title'] = 'faq';
        $this->data['content'] = 'home/faq';
        $this->_render_page($this->template, $this->data);
    }
     public function service_first(){
        $this->data['title'] = 'service_first';
        $this->data['content'] = 'home/service_first';
        $this->_render_page($this->template, $this->data);
    }
     public function service_second(){
        $this->data['title'] = 'service_second';
        $this->data['content'] = 'home/service_second';
        $this->_render_page($this->template, $this->data);
    }
     public function service_third(){
        $this->data['title'] = 'service_three';
        $this->data['content'] = 'home/service_third';
        $this->_render_page($this->template, $this->data);
    }

     public function contact()
    {
        $this->form_validation->set_rules($this->contact_model->rules);

        if ($this->form_validation->run() === false) {
            $this->data['title'] = 'Contact Us';
            $this->data['content'] = 'home/contact';
            $this->_render_page($this->template, $this->data);
        } else {

            
            $id = $this->contact_model->insert([
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'subject' => $this->input->post('subject'),
                'message' => $this->input->post('message')
            ]);
            if (! empty($id))
                $this->session->set_flashdata('success', 'Contact Details Submitted Successfully..!');
            else
                $this->session->set_flashdata('error', 'Something went wrong..!');

            redirect('home', 'refresh');
        }
    }
    public function privacy(){
        $this->data['title'] = 'privacy';
        $this->data['content'] = 'home/privacy';
        $this->_render_page($this->template, $this->data);
    }
  
     
   public function subscribe()
    {
        $id = $this->Subscribe_model->insert([
            'email' => $this->input->post('email')
        ]);
        if (! empty($id))
            $this->session->set_flashdata('subscribed', 'Successfully Subscribed');
        else
            $this->session->set_flashdata('error', 'Something went wrong..!');
        redirect('home', 'refresh');
    }
    
   
}

