<section class="question_area section-gap">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="about-video-wrapper">
                            <div class="popup-videos wow pixFadeRight">
                                <div class="video-thumbnail">
                                    <img src="<?php echo base_url();?>assets/img/blog/blog-page/single_blog_3.jpg" alt="saaspik"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="about-tax-content">
                            
                                <img src="<?php echo base_url();?>assets/img/blog/blog-page/bradcam.png" alt="Its about Us!">
                                <div class="centered">Its about Us!</div>
                          
                            <p class="wow pixFadeUp" data-wow-delay="0.3s">Next Click Info Solutions Private Limited provides global clients with a distinctive offer – the blend of Business Process Outsourcing and Superior Technology – in a single entity through our offshore development facility in India &USA. The outcome is better process mechanization and technical developments that can trim down operational expenses for your business. The NEXTCLICK is a Software Service company having presence in vast field of web services, Custom Application Development.!</p>

                            <p class="description wow pixFadeUp" data-wow-delay="0.4s">NEXTCLICK came into existence from 22nd Aug 2019, by acquiring Innovative Software’s. Starting with inorganic growth is a well thought out step that forges our strategy to grow by access to an already proven talent pool, international presence and in-house expertise of Innovative Software’s.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="question_area section-gap" id="faq">
        <div class="container">
            <div class="section-title">
                <h2>Our  <em class="gradient-bg">Vision</em></h2>
            </div>
            <div class="row align-items-center justify-content-center">
                <div class="col-md-5">
                    <div class="right_question_img">
                        <img src="assets/img/logo.png" alt="soura">
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="left_question_inner">
                        <div class="accordion" id="accordion">
                            <div class="card">
                                <div class="card-heading" id="headingOne">
                                  
                                </div>
                                <div id="collapseOne" class="collapse show" data-parent="#accordion" aria-labelledby="headingOne">
                                    <div class="card-body">Our vision is to become businesses’ first choice when it comes to software development and maintenance. To accomplish this, we always try to exceed our client’s expectations. NEXT CLICK strives to build lasting partnerships and ensures client satisfaction. !</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   <section class="question_area section-gap" id="faq">
        <div class="container">
            <div class="section-title">
                <h2>Our  <em class="gradient-bg">Mision</em></h2>
            </div>
            <div class="row align-items-center justify-content-center">
                <div class="col-md-7">
                    <div class="left_question_inner">
                        <div class="accordion" id="accordion">
                            <div class="card">
                                <div class="card-heading" id="headingOne">
                                  
                                </div>
                                <div id="collapseOne" class="collapse show" data-parent="#accordion" aria-labelledby="headingOne">
                                    <div class="card-body">Our mission is to provide innovative software solutions for excellence and enterprise compliance.!</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="right_question_img">
                        <img src="assets/img/logo.png" alt="soura">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="team-area section-gap" id="team">
        <div class="container">
            <div class="section-title">
                <h2>Meet my <em class="gradient-bg">team</em></h2>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 team-container">
                    <div class="team-item">
                        <div class="pic">
                            <img src="assets/img/team/img1.jpg" alt="team image">
                        </div>
                        <div class="team-content">
                            <h3 class="title">Sara</h3>  <span class="post">Web Developer</span>
                        </div>
                        <ul class="social transition">
                            <li>
                                <a href="#" class="fa fa-facebook"></a>
                            </li>
                            <li>
                                <a href="#" class="fa fa-twitter"></a>
                            </li>
                            <li>
                                <a href="#" class="fa fa-google-plus"></a>
                            </li>
                            <li>
                                <a href="#" class="fa fa-linkedin"></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 team-container">
                    <div class="team-item">
                        <div class="pic">
                            <img src="assets/img/team/img2.jpg" alt="team image">
                        </div>
                        <div class="team-content">
                            <h3 class="title">Ahmed</h3>  <span class="post">Web Designer</span>
                        </div>
                        <ul class="social transition">
                            <li>
                                <a href="#" class="fa fa-facebook"></a>
                            </li>
                            <li>
                                <a href="#" class="fa fa-twitter"></a>
                            </li>
                            <li>
                                <a href="#" class="fa fa-google-plus"></a>
                            </li>
                            <li>
                                <a href="#" class="fa fa-linkedin"></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 team-container">
                    <div class="team-item">
                        <div class="pic">
                            <img src="assets/img/team/img3.jpg" alt="team image">
                        </div>
                        <div class="team-content">
                            <h3 class="title">Karim</h3>  <span class="post">General Manager</span>
                        </div>
                        <ul class="social transition">
                            <li>
                                <a href="#" class="fa fa-facebook"></a>
                            </li>
                            <li>
                                <a href="#" class="fa fa-twitter"></a>
                            </li>
                            <li>
                                <a href="#" class="fa fa-google-plus"></a>
                            </li>
                            <li>
                                <a href="#" class="fa fa-linkedin"></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 team-container">
                    <div class="team-item">
                        <div class="pic">
                            <img src="assets/img/team/img4.jpg" alt="team image">
                        </div>
                        <div class="team-content">
                            <h3 class="title">Nextclick</h3>  <span class="post">CEO</span>
                        </div>
                        <ul class="social transition">
                            <li>
                                <a href="#" class="fa fa-facebook"></a>
                            </li>
                            <li>
                                <a href="#" class="fa fa-twitter"></a>
                            </li>
                            <li>
                                <a href="#" class="fa fa-google-plus"></a>
                            </li>
                            <li>
                                <a href="#" class="fa fa-linkedin"></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style type="text/css">
        /* Centered text */
.centered {
  position: absolute;
  top: 18%;
  left: 50%;
  transform: translate(-50%, -50%);
}
    </style>