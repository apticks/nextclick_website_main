
    <section class="question_area section-gap" id="faq">
        <div class="container">
            <div class="section-title">
                <h2>Common <em class="gradient-bg">Question</em></h2>
            </div>
            <div class="row align-items-center justify-content-center">
                <div class="col-md-6">
                    <div class="left_question_inner">
                        <div class="accordion" id="accordion">
                            <div class="card">
                                <div class="card-heading" id="headingOne">
                                    <h4> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> 1. What is OnTouch Apps ? <i class="fa fa-minus gradient-bg" aria-hidden="true"></i> <i class="fa fa-plus gradient-bg" aria-hidden="true"></i> </a></h4>
                                </div>
                                <div id="collapseOne" class="collapse show" data-parent="#accordion" aria-labelledby="headingOne">
                                    <div class="card-body">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, lege in Virginia,</div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-heading" id="headingTwo">
                                    <h4> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> 2. How can I use it ? <i class="fa fa-minus gradient-bg" aria-hidden="true"></i> <i class="fa fa-plus gradient-bg" aria-hidden="true"></i> </a></h4>
                                </div>
                                <div id="collapseTwo" class="collapse" data-parent="#accordion" aria-labelledby="headingTwo">
                                    <div class="card-body">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, lege in Virginia,</div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-heading" id="headingThree">
                                    <h4> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> 3. Which operating system is best ? <i class="fa fa-minus gradient-bg" aria-hidden="true"></i> <i class="fa fa-plus gradient-bg" aria-hidden="true"></i> " </a></h4>
                                </div>
                                <div id="collapseThree" class="collapse" data-parent="#accordion" aria-labelledby="headingThree">
                                    <div class="card-body">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, lege in Virginia,</div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-heading" id="headingfour">
                                    <h4> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour"> 4. Where can I get rome ? <i class="fa fa-minus gradient-bg" aria-hidden="true"></i> <i class="fa fa-plus gradient-bg" aria-hidden="true"></i> </a></h4>
                                </div>
                                <div id="collapsefour" class="collapse" data-parent="#accordion" aria-labelledby="headingfour">
                                    <div class="card-body">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, lege in Virginia,</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="right_question_inner">
                        <div class="accordion" id="accordion1">
                            <div class="card">
                                <div class="card-heading" id="headingFive">
                                    <h4> <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseFive" aria-expanded="true" aria-controls="collapseOne"> 5. What is New Features Apps ? <i class="fa fa-minus gradient-bg" aria-hidden="true"></i> <i class="fa fa-plus gradient-bg" aria-hidden="true"></i> </a></h4>
                                </div>
                                <div id="collapseFive" class="collapse show" data-parent="#accordion1" aria-labelledby="headingFive">
                                    <div class="card-body">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, lege in Virginia,</div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-heading" id="headingSix">
                                    <h4> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix"> 6. How can I upgrade ? <i class="fa fa-minus gradient-bg" aria-hidden="true"></i> <i class="fa fa-plus gradient-bg" aria-hidden="true"></i> </a></h4>
                                </div>
                                <div id="collapseSix" class="collapse" data-parent="#accordion1" aria-labelledby="headingSix">
                                    <div class="card-body">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, lege in Virginia,</div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-heading" id="headingSeven">
                                    <h4> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven"> 7. Which operating system is Nextclick ? <i class="fa fa-minus gradient-bg" aria-hidden="true"></i> <i class="fa fa-plus gradient-bg" aria-hidden="true"></i> </a></h4>
                                </div>
                                <div id="collapseSeven" class="collapse" data-parent="#accordion1" aria-labelledby="headingSeven">
                                    <div class="card-body">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, lege in Virginia,</div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-heading" id="headingEight">
                                    <h4> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight"> 8. Where can I get Accessores ? <i class="fa fa-minus gradient-bg" aria-hidden="true"></i> <i class="fa fa-plus gradient-bg" aria-hidden="true"></i> </a></h4>
                                </div>
                                <div id="collapseEight" class="collapse" data-parent="#accordion1" aria-labelledby="headingEight">
                                    <div class="card-body">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, lege in Virginia,</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>