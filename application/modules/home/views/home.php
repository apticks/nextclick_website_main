<?php if(!empty($this->session->flashdata('success'))){ ?>

    <script type='text/javascript'>
        window.alert('Successfully success!!!')
    </script>

    <?php }?>
    <div class="welcome-area transition" id="welcome">
        <div class="header-text">
            <div class="container">
                <div class="row fullscreen d-flex align-items-center">
                    <div class="left-text col-lg-6 col-md-12 justify-content-center" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                        <h1> Nextclick<br> Suits You Better</h1>
                        <p>Lorem ipsum dolor sit amet, consectetr adipisicing elit, sed do eiusmod tempor incididunt</p>
                        <div class="download-button d-flex flex-row justify-content-start">
                            <div class="buttons transition flex-row d-flex"> <i class="fa fa-apple"></i>
                                <div class="desc">
                                    <a href="#">
                                        <p> <span>Available</span> 
                                            <br>on App Store</p>
                                    </a>
                                </div>
                            </div>
                            <div class="buttons transition gradient-bg flex-row d-flex"> <i class="fa fa-android text-white"></i>
                                <div class="desc">
                                    <a class="text-white" href="#">
                                        <p> <span>Available</span> 
                                            <br>on Play Store</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="banner-images col-lg-6 col-md-6 align-self-end">
                        <div class="row animated_images">
                            <ul>
                                <li>
                                    <img src="assets/img/welcome-area/img1.png" alt="soura" data-parallax='{"x": 50, "y": -100}'>
                                </li>
                                <li>
                                    <img src="assets/img/welcome-area/img2.png" alt="soura">
                                </li>
                                <li>
                                    <img src="assets/img/welcome-area/img3.png" alt="soura" data-parallax='{"x": 25, "y": -50}'>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="brands-area">
        <div class="container">
            <div class="brand-wrap">
                <div class="row align-items-center active-brand-carusel justify-content-start">
                    <div class="col single-brand transition">
                        <a href="#">
                            <img class="mx-auto" src="assets/img/brand/b1.png" alt="brand">
                        </a>
                    </div>
                    <div class="col single-brand transition">
                        <a href="#">
                            <img class="mx-auto" src="assets/img/brand/b2.png" alt="brand">
                        </a>
                    </div>
                    <div class="col single-brand transition">
                        <a href="#">
                            <img class="mx-auto" src="assets/img/brand/b3.png" alt="brand">
                        </a>
                    </div>
                    <div class="col single-brand transition">
                        <a href="#">
                            <img class="mx-auto" src="assets/img/brand/b4.png" alt="brand">
                        </a>
                    </div>
                    <div class="col single-brand transition">
                        <a href="#">
                            <img class="mx-auto" src="assets/img/brand/b5.png" alt="brand">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <section class="section-gap" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 about-container" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <div class="about-item">
                        <div class="about-icon"> <i class="lnr lnr-magnifier"></i>
                        </div>
                        <div class="about-content">
                            <h3>Trend Analysis</h3>
                            <p>Lorem ipsum dolor sit amet, consectetr adipisicing elit, sed do eiusmod tempor incididunt.</p> <a href="#">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 about-container" data-scroll-reveal="enter bottom move 30px over 0.6s after 0.4s">
                    <div class="about-item">
                        <div class="about-icon"> <i class="lnr lnr-thumbs-up"></i>
                        </div>
                        <div class="about-content">
                            <h3>Site Optimization</h3>
                            <p>Lorem ipsum dolor sit amet, consectetr adipisicing elit, sed do eiusmod tempor incididunt.</p> <a href="#">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 about-container" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                    <div class="about-item">
                        <div class="about-icon"> <i class="lnr lnr-envelope"></i>
                        </div>
                        <div class="about-content">
                            <h3>Email Design</h3>
                            <p>Lorem ipsum dolor sit amet, consectetr adipisicing elit, sed do eiusmod tempor incididunt.</p> <a href="#">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="left-image-decor"></div>
    <div class="video-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="video-area">
                        <div class="video-play-btn">
                            <a href="https://www.youtube.com/watch?v=jNJCdxMf0t8" class="video_btn gradient-bg">
                                <img src="assets/img/video-area/button.png" alt="play button">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="achivement-area text-center gradient-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 achive-container">
                    <div class="achive-single">
                        <div class="icon"> <i class="lnr lnr-cloud-download"></i>
                        </div>
                        <h2> <span class="counter">20</span>K</h2>  <span>Download</span>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 achive-container">
                    <div class="achive-single">
                        <div class="icon"> <i class="lnr lnr-heart-pulse"></i>
                        </div>
                        <h2> <span class="counter">5</span>K</h2>  <span>Happy Clients</span>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 achive-container">
                    <div class="achive-single">
                        <div class="icon"> <i class="lnr lnr-coffee-cup"></i>
                        </div>
                        <h2> <span class="counter">30</span>K</h2>  <span>Coffee Cups</span>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 achive-container">
                    <div class="achive-single">
                        <div class="icon"> <i class="fa fa-trophy"></i>
                        </div>
                        <h2> <span class="counter">50</span></h2>  <span>Award</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="app-screenshots-area section-gap gradient-bg" id="screen-shots">
        <div class="container">
            <div class="section-title">
                <h2 class="text-white">Nextclick ScreenShots</h2>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="screenshot-wrap">
                        <div class="single-screenshot">
                            <img src="assets/img/scr-img/screenshot1.jpg" alt="screenshot" />
                        </div>
                        <div class="single-screenshot">
                            <img src="assets/img/scr-img/screenshot2.jpg" alt="screenshot" />
                        </div>
                        <div class="single-screenshot">
                            <img src="assets/img/scr-img/screenshot3.jpg" alt="screenshot" />
                        </div>
                        <div class="single-screenshot">
                            <img src="assets/img/scr-img/screenshot4.jpg" alt="screenshot" />
                        </div>
                        <div class="single-screenshot">
                            <img src="assets/img/scr-img/screenshot5.jpg" alt="screenshot" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="right-image-decor"></div>
    <section class="testimonials_area section-gap" id="review">
        <div class="container">
            <div class="section-title">
                <h2>Happy <em class="gradient-bg">Customers</em></h2>
            </div>
            <div class="row">
                <div class="testi-slide">
                    <div class="testi-box">
                        <div class="testi-image">
                            <img src="assets/img/testimonial/img1.jpg" alt="Soura">
                        </div>
                        <h4>Sara Ezzat</h4>  <span class="position">Art Dirrector</span>
                        <p>Lorem ipsum dolor sit amet, conseg sed do eiusmod temput laborelaborus ed sed do eiusmod tempo.</p>
                    </div>
                    <div class="testi-box">
                        <div class="testi-image">
                            <img src="assets/img/testimonial/img2.jpg" alt="Soura">
                        </div>
                        <h4>Karim Ezzat</h4>  <span class="position">Web Developer</span>
                        <p>Lorem ipsum dolor sit amet, conseg sed do eiusmod temput laborelaborus ed sed do eiusmod tempo.</p>
                    </div>
                    <div class="testi-box">
                        <div class="testi-image">
                            <img src="assets/img/testimonial/img3.jpg" alt="Soura">
                        </div>
                        <h4>Ahmed Ezzat</h4>  <span class="position">Web Designer</span>
                        <p>Lorem ipsum dolor sit amet, conseg sed do eiusmod temput laborelaborus ed sed do eiusmod tempo.</p>
                    </div>
                    <div class="testi-box">
                        <div class="testi-image">
                            <img src="assets/img/testimonial/img4.jpg" alt="Soura">
                        </div>
                        <h4>Nextclick</h4>  <span class="position">CEO</span>
                        <p>Lorem ipsum dolor sit amet, conseg sed do eiusmod temput laborelaborus ed sed do eiusmod tempo.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>