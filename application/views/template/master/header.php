<!-- Latest compiled and minified CSS --><!-- 
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

<!-- Optional theme 
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<div class="gradient-bg" id="preloader">
		<div class="loader"></div>
	</div>
	<header class="header-area transition header-sticky">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<nav class="main-nav"> <a href="<?php echo base_url('home');?>" class="logo transition"><img src="<?php echo base_url()?>assets/img/logo.png" alt="site logo" ></a>
						<ul class="nav transition dp">
							<li><a href="<?php echo base_url('home');?>" >Home</a>
							</li>
							<li><a href="<?php echo base_url('about_us');?>"  >About</a>
							</li>
							
							
							<li><a href="<?php echo base_url('faq');?>" >FAQ</a>
							</li>
							<li class="scroll-to-section"><a href="#contact" class="menu-item">Contact Us</a>
							</li>
							<!-- <li class="scroll-to-section"><a href="#screen-shots" class="menu-item">Gallery</a>
							</li> -->

            <li>
              <a href="<?php echo base_url('faq');?>" class="dropdown-toggle" data-toggle="dropdown">Services <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url('service_first');?>">Services 1</a></li>
                <li><a href="<?php echo base_url('service_second');?>">Services 2</a></li>
                <li><a href="<?php echo base_url('service_third');?>">Services 3</a></li>
              </ul>
            </li>
          
						</ul>
						<a class='menu-trigger'> <span class="transition">Menu</span> 
						</a>




					</nav>
				</div>
			</div>
		</div>
	</header>
	<!-- <style type="text/css">
   nav {
  margin-top: 60px;
  box-shadow: 5px 4px 5px #000;
} 
  </style>
  <script type="text/javascript">
   $('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});
  </script> -->
  <style type="text/css">
  .dropdown-menu a:hover {background-color: #ddd;}

.dp:hover .dropdown-menu {text-align: center;
  padding: 14px 16px;
  text-decoration: none;}

.dp:hover .dropdown-toggle {background-color: white;}
  </style>