<script src="<?php echo base_url();?>assets/js/jquery-3.2.0.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/swiper.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/scrollreveal.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.counterup.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.waypoints.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/slick.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/magnific-popup.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.parallax-scroll.js"></script>
	<script src="<?php echo base_url();?>assets/js/theme.js"></script>

	<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5f61cad4f0e7167d0010beca/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->