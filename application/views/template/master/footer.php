
			<section class="call-to-action action-padding">
			
			<div class="contact-form">
				<div class="row align-items-center justify-content-between">
					<div class="col-lg-7">
						<div class="action-content style-two wow pixFadeUp">
							<h2 class="title">Get update from anywheres<br>& subscribe us to get more info</h2>
						</div>
					</div>
					<form action="<?php echo base_url('subscribe');?>"  method="post" enctype="multipart/form-data" class="newsletter-form wow pixFadeUp" data-pixsaas="newsletter-subscribe">
                            
                                <input type="email" name="email" class="input"  placeholder="Enter your Email" required>
                                <input class="transition gradient-bg" type="submit" id="send" value="Subscribe">
                          
                            <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                            <div class="clearfix"></div>
                            <div class="form-result alert">
                                <div class="content"></div>
                            </div>
                    </form><!-- 
                    <div class="contact-form">
						<h4 class="contact-title">Get In Touch</h4>
						<form action="#" method="post">
							<input type="text" class="input" name="name" placeholder="Your Name">
							<input type="email" class="input" name="email" placeholder="Your Email address">
							<input type="text" class="input" name="subject" placeholder="Your Subject">
							<textarea name="msg" class="input" id="msg" placeholder="Your Message"></textarea>
							<input class="transition gradient-bg" type="submit" id="send" value="Send">
						</form>
					</div> -->
				</div>
			</div>
		</section>
			<a href="javascript:;" id="scrollUp">
		<img src="assets/img/scroll-up.png" alt="soura">
	</a><footer class="footer-area">
		<div class="container">
			<div class="contact-area section-gap" id="contact">
				<div class="row">
					<div class="col-lg-6 col-md-12">
						<div class="contact-container">
							<div class="contact-form">
								<form action="<?php echo base_url('contact');?>" method="post" enctype="multipart/form-data">
									<input type="text" class="input" name="name" placeholder="Your Name">
									<input type="email" class="input" name="email" placeholder="Your Email address">
									<input type="text" class="input" name="subject" placeholder="Your Subject">
									<textarea name="message" class="input" id="msg" placeholder="Your Message"></textarea>
									<input class="transition gradient-bg" type="submit" id="send" value="Send">
								</form>
							</div>
						</div>
					</div>
					<div class="right-content col-lg-6 col-md-12">
						<h2>More About Nextclick</h2>
						<p class="text-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
							<br>
							<br>If you need this contact form to send email to your inbox, you may follow our contact page for more detail.</p>
						<ul class="social">
							<li><a href="#"><i class="fa fa-facebook"></i></a>
							</li>
							<li><a href="#"><i class="fa fa-twitter"></i></a>
							</li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a>
							</li>
							<li><a href="#"><i class="fa fa-rss"></i></a>
							</li>
							<li><a href="#"><i class="fa fa-dribbble"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="footer-inner">
				<p class="copy-right text-white">Copyright &copy;
					<script>
						document.write(new Date().getFullYear());
					</script>All rights reserved | This template is made with <i class="lnr lnr-heart-pulse"></i> by <a class="text-white" href="https://themeforest.net/user/karimezzat" target="_blank">***********</a>
				</p>
			</div>
		</div>
	</footer>