<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <title>Nextclick</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="Karim Ezzat">
    <meta name="description" content="One Page Creative App HTML5 Template">
    <meta name="keywords" content="onepage, responsive, app, mobile, android, html5, css3, css3 animation">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700,900">
</head>
<?php $this->load->view('template/master/topcss');?>
<body>
<!-- navbar area start -->
	<?php $this->load->view('template/master/header');?>
<!-- navbar area end -->

<!-- Content area start -->
	<?php $this->load->view($content);?>
<!-- Content area end -->

<!-- footer area start -->
	<?php $this->load->view('template/master/footer');?>
<!-- footer area end -->


  <a  class="whats-app" href="https://api.whatsapp.com/send?phone=917989331018&text=Hi%20There!" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
    </a>

<?php $this->load->view('template/master/scripts');?>
</body>
</html>
<style type="text/css">
    .whats-app {
    position: fixed;
    width: 60px;
    height: 60px;
    bottom: 22px;
    left: 15px;
    background-color: #25d366;
    color: #FFF;
    border-radius: 50px;
    text-align: center;
    font-size: 30px;
    box-shadow: 2px 2px 3px #999;
    z-index: 100;
}
.my-float {
    margin-top: 16px;
}
</style>